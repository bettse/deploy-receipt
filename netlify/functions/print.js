const util = require('util');
const fs = require('fs');
const fetch = require('node-fetch');
const PDFDocument = require('pdfkit');
const ipp = require('ipp');
const QRCode = require('qrcode');

const {NETLIFY_DEV, RECEIPT_PRINTER_URI} = process.env;

//media=custom_71.97x99.84mm_71.97x99.84mm'
const receiptPt = {
  height: 283,
  width: 204,
};
const margin = 10;

const logoSize = {
  width: receiptPt,
  height: 50,
};

async function printReceipt(pdf) {
  const receiptPrinter = ipp.Printer(RECEIPT_PRINTER_URI);
  const execute = util.promisify(receiptPrinter.execute.bind(receiptPrinter));

  try {
    if (NETLIFY_DEV) {
      const attrs = await execute('Get-Printer-Attributes', null);
      console.log(attrs['printer-attributes-tag']);
      fs.writeFileSync('./receipt.pdf', pdf);
      return;
    }

    // 'custom_71.97x99.84mm_71.97x99.84mm',
    // 'custom_71.97x199.67mm_71.97x199.67mm',
    // 'custom_71.97x1499.66mm_71.97x1499.66mm',
    const request = {
      'operation-attributes-tag': {
        'requesting-user-name': 'bettse@fastmail.fm',
        'job-name': 'receipt.pdf',
        'document-format': 'application/pdf',
      },
      'job-attributes-tag': {
        media: 'custom_71.97x99.84mm_71.97x99.84mm',
        'orientation-requested': 3,
      },
      data: pdf,
    };

    console.log('request', request);
    const result = execute('Print-Job', request);
    console.log('result', await result);
  } catch (e) {
    console.error('error printing', e);
  }
}

async function composePdf(event) {
  const {
    id,
    published_at,
    site_id,
    name,
    url,
    deploy_url,
    commit_ref,
    branch,
    commit_url,
    title,
    committer,
  } = event;

  const logo = await fetch(
    'https://www.netlify.com/img/press/logos/full-logo-light.png',
  ).then(res => res.buffer());

  return new Promise(async (resolve, reject) => {
    const doc = new PDFDocument({
      size: [receiptPt.width, receiptPt.height],
      margin,
    });

    let buffers = [];
    doc.on('error', reject); //dunno if it actually emits this
    doc.on('data', buffers.push.bind(buffers));
    doc.on('end', () => {
      const pdfData = Buffer.concat(buffers);
      resolve(pdfData);
    });

    doc.image(logo, {
      width: receiptPt.width - margin * 2,
      align: 'center',
      valign: 'center',
    });

    doc.text(name, 0, logoSize.height + margin, {
      align: 'center',
      width: receiptPt.width,
      height: receiptPt.height,
    });
    doc.fontSize(8);
    doc.text(url, {
      align: 'center',
    });
    doc.text(site_id, {
      align: 'center',
    });

    const date = new Date(published_at).toDateString();
    const time = new Date(published_at).toLocaleTimeString('en-US');

    doc.moveDown();
    doc.text(`Branch: ${branch}`, {
      align: 'left',
      continued: true,
    });
    doc.text(`${date}`, {
      align: 'right',
    });

    doc.text(`Committer: ${committer}`, {
      align: 'left',
      continued: true,
    });
    doc.text(`${time}`, {
      align: 'right',
    });
    doc.moveDown();

    // -------
    doc
      .moveTo(margin, doc.y)
      .lineTo(receiptPt.width - margin - margin, doc.y)
      .dash(5, {space: 5})
      .stroke();
    doc.moveDown();

    const fields = [
      'title',
      'manual_deploy',
      'framework',
      'plugin_state',
      'has_edge_handlers',
    ];
    const titles = fields
      .map(field => ` • ${field}: ${event[field]}`)
      .join('\n');
    // Prevent longer messages from blowing out the length
    doc.text(titles, {height: 6 * 8});
    doc.moveDown();

    // -------
    doc
      .moveTo(margin, doc.y)
      .lineTo(receiptPt.width - margin - margin, doc.y)
      .dash(5, {space: 5})
      .stroke();

    const qr = await QRCode.toDataURL(deploy_url);
    const qrSize = 64;
    doc.image(qr, receiptPt.width / 2 - qrSize / 2, doc.y, {
      fit: [qrSize, qrSize],
      align: 'center',
      valign: 'center',
      link: deploy_url,
    });

    const fontSize = 12;
    doc.text(id, 0, receiptPt.height - margin - fontSize, {
      align: 'center',
    });

    doc.end();
  });
}

const success = {
  statusCode: 204,
  body: '',
};

exports.handler = async function(event, context) {
  try {
    const body = JSON.parse(event.body);
    if (body.context !== 'production') {
      console.log(`skipping context ${body.context}`);
      return;
    }
    const pdf = await composePdf(body);
    await printReceipt(pdf);
  } catch (e) {
    console.log('error in handler', e);
    // Because this is used as a webhook, always return success
    return success;
  }
  return success;
};
