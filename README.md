# Deploy Receipt

- Have you longed for a physical token to represent the digital act of deploying?
- Does your office have archaic rules around preserving records?
- Do you like annoying Eric with printer noises?

If you said yes to any of these questions, I have the solution: Deploy Receipt!

[![Netlify Status](https://api.netlify.com/api/v1/badges/5ad138e6-2c0b-4aed-bb91-1681601adfdf/deploy-status)](https://app.netlify.com/sites/deploy-receipt/deploys)

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
