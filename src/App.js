import React, {useState, useEffect} from 'react';

import {When} from 'react-if';

import Nav from 'react-bootstrap/Nav';
import Navbar from 'react-bootstrap/Navbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Jumbotron from 'react-bootstrap/Jumbotron';
import Image from 'react-bootstrap/Image';
import Modal from 'react-bootstrap/Modal';
import Card from 'react-bootstrap/Card';
import Accordion from 'react-bootstrap/Accordion';
import ListGroup from 'react-bootstrap/ListGroup';

import './App.css';
import example from './example.png';

function App() {
  const [show, setShow] = useState(false);
  const [printerDetails, setPrinterDetails] = useState({});

  useEffect(() => {
    async function checkStatus() {
      try {
        const resp = await fetch('/.netlify/functions/status');
        setPrinterDetails(await resp.json());
      } catch (e) {
        console.log('checkStatus', e);
      }
    }
    checkStatus();
  }, []);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Navbar collapseOnSelect expand="sm" bg="dark" variant="dark">
        <Navbar.Brand>Deploy Receipt</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            <Nav.Link href="https://gitlab.com/bettse/deploy-receipt/">
              Sourcecode (GitLab)
            </Nav.Link>
          </Nav>
          <Nav>
            <Navbar.Text>
              <img
                src="https://api.netlify.com/api/v1/badges/5ad138e6-2c0b-4aed-bb91-1681601adfdf/deploy-status"
                alt="Netlify Status"
              />
            </Navbar.Text>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <Container fluid="md" className="mt-3">
        <Row>
          <Col>
            <Jumbotron>
              <h1>Deploy Receipt!</h1>
              <ul>
                <li>
                  Have you longed for a physical token to represent the digital
                  act of deploying?
                </li>
                <li>
                  Does your office have archaic rules around preserving records?
                </li>
                <li> Do you like annoying Eric with printer noises?</li>
              </ul>
              <p>
                If you said yes to any of these questions, I have the solution:
                Deploy Receipt!
              </p>
              <p>
                Configure your deploy with a POST webhook to &nbsp;
                <code>
                  {window.location.toString()}.netlify/functions/print
                </code>
                .
              </p>
              <p>
                With each deploy, a receipt, like the following, will be printed
                on Eric's Star Micronics T300i thermal paper receipt printer.
              </p>
              <div className="text-center justify-content-center">
                <Image
                  src={example}
                  width="33%"
                  thumbnail
                  onClick={handleShow}
                />
              </div>
            </Jumbotron>
          </Col>
        </Row>
        <Row>
          <Col>
            <Accordion>
              <Card>
                <Accordion.Toggle as={Card.Header} eventKey="0">
                  Printer Status
                </Accordion.Toggle>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <When condition={Object.keys(printerDetails).length > 0}>
                      <ListGroup variant="flush">
                        <ListGroup.Item
                          variant={
                            printerDetails.state === 'idle' ? '' : 'warning'
                          }>
                          State: {printerDetails.state}
                        </ListGroup.Item>
                        <When condition={printerDetails.message !== ''}>
                          <ListGroup.Item variant="info">
                            Message: {printerDetails.message}
                          </ListGroup.Item>
                        </When>
                        <When condition={printerDetails.reasons !== 'none'}>
                          <ListGroup.Item variant="info">
                            Reasons: {printerDetails.reasons}
                          </ListGroup.Item>
                        </When>
                      </ListGroup>
                    </When>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </Col>
        </Row>
      </Container>
      <Modal show={show} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Example Receipt</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Image src={example} fluid />
        </Modal.Body>

        <Modal.Footer></Modal.Footer>
      </Modal>
      <footer className="footer font-small mx-auto">
        <Container fluid className="text-center">
          <Row noGutters>
            <Col>
              <small className="text-muted">
                Hosted with <a href="https://www.netlify.com/">Netlify</a>
              </small>
            </Col>
          </Row>
        </Container>
      </footer>
    </>
  );
}

export default App;
